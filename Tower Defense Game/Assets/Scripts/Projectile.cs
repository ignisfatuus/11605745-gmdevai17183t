﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public bool hasBeenFired;
    public Vector3 TargetPosition;
    public float Speed;
    public float RangeMultiplier;
    public float xRangeOfProjectile;
    public float yRangeOfProjectile;
    public int Damage;
    // Use this for initialization
    void Start()
    {
        hasBeenFired = false;
        ComputeTargetLocation();
        StartCoroutine(DestroyObjectInSeconds(5));
    }

    // Update is called once per frame
    void Update() {

        MoveToPosition();
    }

    public void MoveToPosition ()

        {
  
        transform.position = Vector3.MoveTowards(transform.position, TargetPosition, Speed);
     
        }

    public void ComputeTargetLocation()
    {


        ////get position of mouse input and direct the object towards it
        TargetPosition  = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //if (hasBeenFired) return;
        //TargetPosition.Normalize();


        TargetPosition.z = 0;

        //Debug.Log(TargetPosition);
        ////Multiply it to the range
        //xRangeOfProjectile = gameObject.transform.position.x;
        //yRangeOfProjectile = gameObject.transform.position.y;
        //xRangeOfProjectile += RangeMultiplier;
        //yRangeOfProjectile += RangeMultiplier;
        //TargetPosition.x *= xRangeOfProjectile;
        //TargetPosition.y *= xRangeOfProjectile;
        //Debug.Log(TargetPosition);
        hasBeenFired = true;
    }


    private void OnTriggerEnter(Collider collision)
    {
        Debug.Log("Hello");
        collision.GetComponent<Health>().TakeDamage(Damage);
        Destroy(gameObject);
    }

    IEnumerator DestroyObjectInSeconds(int seconds)
    {

        yield return new WaitForSeconds(seconds);
        Destroy(gameObject);

    }

}
