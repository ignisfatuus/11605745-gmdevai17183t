﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public int MaxHp;
    public int CurrentHp;
    public bool Alive { get { return CurrentHp > 0; } }
    public float Normalized { get { return (float)CurrentHp / MaxHp; } }
    // Use this for initialization

    void Start()
    {
        CurrentHp = MaxHp;
    }

    private void OnEnable()
    {
        CurrentHp = MaxHp;
    }
    // Update is called once per frame
    void Update()
    {
        CheckDeath();
    }

    public void TakeDamage(int damage)
    {
        CurrentHp -= damage;
        Debug.Log(CurrentHp);
    }

    public void CheckDeath()
    {
        if (CurrentHp <= 0)
        {
            CurrentHp = 0;
            gameObject.SetActive(false);
        }
    }
 
}
