﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node  {

    // The position in the Node Array
    public int GridPosX;
    public int GridPosY;

    // Tells the algorithm if a node is being obstructed
    public bool IsWall;

    // The world position of the node
    public Vector3 Position;

    // Stores the previous node that it came from so that we can trace the shortest path

    public Node ParentNode;

    // The cost of moving to the next node
    public int G_Cost;
    //The distance from the goal to this node (HEURISTIC)
    public int H_cost;

    // A Star formula = F(N) = G(N) + H(N)
    public int F_COST
    {
        get { return G_Cost + H_cost; }
    }

    public Node(bool isWall, Vector3 worldPos, int gridX, int gridY)
    {
        this.IsWall = isWall;
        this.Position = worldPos;
        this.GridPosX = gridX;
        this.GridPosY = gridY;
    }
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
