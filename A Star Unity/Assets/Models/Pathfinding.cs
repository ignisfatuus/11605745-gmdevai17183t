﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

    public Grid grid;
    public Transform startPos, targetpos;

    private void Awake()
    {
        grid = GetComponent<Grid>();
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        FindPath(startPos.position, targetpos.position);
    }

    void FindPath(Vector3 start, Vector3 target)
    {
        Node startNode = grid.NodeFromWorldPoint(start);
        Node targetNode = grid.NodeFromWorldPoint(target);
        List<Node> openNodes = new List<Node>();
        HashSet<Node> closedNodes = new HashSet<Node>();

        openNodes.Add(startNode); //Add the start node to the open list to begin the algorithm

        while (openNodes.Count > 0) //While there is something in the open list
        {
            Node currentNode = openNodes[0]; // Create a node and set it to the fisrt item in the open nodes

            for (int i = 1; i < openNodes.Count; i++) // Loop through the open list starting from the second object
            {
                if (openNodes[i].F_COST < currentNode.F_COST || openNodes[i].F_COST == currentNode.F_COST && openNodes[i].H_cost < openNodes[i].H_cost) // check costs and heuristic
                {
                    currentNode = openNodes[i];
                }
            }

            openNodes.Remove(currentNode);
                closedNodes.Add(currentNode);
                if (currentNode == targetNode)
                {
                    GetFinalPath(startNode, targetNode);
                }


                //obstacle detection
                foreach (Node neightbor in grid.GetNeighboringNodes(currentNode)) // loop through each neighbor of the current node
                {
                    if (!neightbor.IsWall || closedNodes.Contains(neightbor))
                    {
                        continue; // skip node
                    }

                    int moveCost = currentNode.G_Cost + GetManhattanDistance(currentNode, neightbor); // Get the F cost of the neighbor
                    if (moveCost < neightbor.G_Cost || !openNodes.Contains(neightbor)) // if the F cost is greater than the G cost or it is not in the open list
                    {
                        neightbor.G_Cost = moveCost;
                        neightbor.H_cost = GetManhattanDistance(neightbor, targetNode);
                        neightbor.ParentNode = currentNode;

                        if(!openNodes.Contains(neightbor))
                        {
                            openNodes.Add(neightbor);
                        }
                    }

                }
            
        }
    }
    void GetFinalPath(Node start, Node end)
    {
        List<Node> FinalPath = new List<Node>();
        Node currentNode = end;

        while (currentNode != start)
        {
            FinalPath.Add(currentNode);
            currentNode = currentNode.ParentNode;

        }

        FinalPath.Reverse();
        grid.FinalPath = FinalPath;
    }

    int GetManhattanDistance(Node nodeA, Node nodeB)
    {
        int x = Mathf.Abs(nodeA.GridPosX - nodeB.GridPosX);
        int y = Mathf.Abs(nodeA.GridPosY - nodeB.GridPosY);

        return x + y;
    }

    


}
